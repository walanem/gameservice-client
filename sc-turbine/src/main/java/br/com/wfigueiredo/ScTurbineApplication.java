package br.com.wfigueiredo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScTurbineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScTurbineApplication.class, args);
	}
}
