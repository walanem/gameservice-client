package br.com.wfigueiredo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by walanem on 06/04/17.
 */
@Data
@AllArgsConstructor
public class Console {

    private String id;

    private String name;

    private Publisher publisher;
}
