package br.com.wfigueiredo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Data
@AllArgsConstructor
public class Game {

    private String id;

    private String name;

    private String genre;

    private Publisher publisher;

    private List<Console> consoles;
}
