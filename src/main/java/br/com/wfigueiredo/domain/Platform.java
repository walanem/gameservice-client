package br.com.wfigueiredo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by walanem on 17/04/17.
 */
@Data
@AllArgsConstructor
public class Platform {

    private String id;

    private String name;

}
