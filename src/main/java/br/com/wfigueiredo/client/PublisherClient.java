package br.com.wfigueiredo.client;

import br.com.wfigueiredo.domain.Publisher;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rx.Observable;

import java.util.List;

/**
 * Created by walanem on 17/04/17.
 */
@FeignClient("${serviceconfig.client.serviceId}")
public interface PublisherClient {

    @Cacheable(value = "publishers")
    @RequestMapping(value = "/publishers", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<List<Publisher>> getPublishers();

    @Cacheable(value = "publisher", key = "#name")
    @RequestMapping(value = "/publisher/{name}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Publisher> getPublisherByName(@PathVariable(value = "name") String name);


}